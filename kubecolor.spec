%define debug_package %{nil}

Name:           kubecolor
Version:        0.0.25
Release:        0%{?dist}
Summary:        Colorize your kubectl output

License:        MIT
URL:            https://github.com/dty1er/kubecolor
Source0:        https://github.com/dty1er/kubecolor/releases/download/v%{version}/kubecolor_%{version}_Linux_x86_64.tar.gz

%description
Colorize your kubectl output

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin/
install -p -m 755 %{name} %{buildroot}/usr/bin

%files
/usr/bin/kubecolor

%changelog
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version v0.0.25
* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
